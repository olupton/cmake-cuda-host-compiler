This is a test case showing an issue with the CUDAHOSTCXX environment variable
in CMake.

It uses a valid CUDA host compiler (gcc) and an invalid one (/usr/bin/true) to
illustrate how CMake behaves.

It appears that CUDAHOSTCXX takes precedence over CMAKE_CUDA_HOST_COMPILER on
the command line, even though the documentation states the opposite.

If `nvcc`, `gcc` and `cmake` are available in your `$PATH`, then running
```bash
sh test.sh
```
should return 0 (success) if the bug was reproduced.
