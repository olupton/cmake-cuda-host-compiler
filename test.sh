set -e
gcc -v
nvcc -V
prep() {
  rm -rf $1
  mkdir $1
  cd $1
}
prep cudahostcxx_gcc
export CUDAHOSTCXX=$(command -v gcc)
# gcc is a valid host compiler
cmake ..
cd ..
prep cudahostcxx_broken
export CUDAHOSTCXX=$(command -v true)
# true is not a valid host compiler
! cmake ..
echo "failed as expected"
cd ..
prep cudahostcxx_broken_cmake_cuda_host_compiler_gcc
export CUDAHOSTCXX=$(command -v true)
# BUG: this should work, because CMAKE_CUDA_HOST_COMPILER should override CUDAHOSTCXX
! cmake -DCMAKE_CUDA_HOST_COMPILER=$(command -v gcc) ..
echo "failed because of a bug!"
cd ..
prep cudahostcxx_unset_cmake_cuda_host_compiler_gcc
unset CUDAHOSTCXX
cmake -DCMAKE_CUDA_HOST_COMPILER=$(command -v gcc) ..
cd ..
